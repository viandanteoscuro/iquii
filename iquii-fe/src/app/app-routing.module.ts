import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
    path: '',
    pathMatch: 'full',
    redirectTo: 'article-list'
  },
  { 
    path: 'article-list', 
    loadChildren: () => import('./modules/article-list/article-list.module')
                  .then(m => m.ArticleListModule) 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

