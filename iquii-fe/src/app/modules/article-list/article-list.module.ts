import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleListRoutingModule } from './article-list-routing.module';
import { ArticleListComponent } from './article-list.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ArticleListComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ArticleListRoutingModule
  ]
})
export class ArticleListModule { }
