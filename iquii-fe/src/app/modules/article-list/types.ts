export interface IArticle {
    name: string,
    link:string,
    tags: string,
    title: string,
    id: number,
    is_favourite_count: number
  }