import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { IArticle } from './types';
import { OrderService } from '../../services/order.service';



@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit {
  /** String to search in article */
  searchText: string = "";

  /** Default model for new article */
  article: IArticle = {
    name: "",
    link: "",
    tags: "",
    title: "",
    id: -1,
    is_favourite_count:0
  };

  /**Sort dir  */
  sorting: string = "asc";

  /** articles list  */
  articles: IArticle[] = [];

  /** shadowed copy of articles, for search purpose */
  _shadowed: IArticle[] = [];

  /** Favourite articles */
  favouriteArticles: IArticle[] = [];

  /** defalt api uri */
  defaultURI: string = "http://iquii-api.local";

  constructor(private apiService: ApiService,private orderService: OrderService) { }

  /**
   * Initial loading of articles
   */
  ngOnInit(): void {
    this.load();
  }
  
  /**
   * Save the new article via async call and reload alla articles
   */
  save() {
    this.apiService.put(this.defaultURI+"/articles", {...this.article})
    .then((res: {status: string, error?:string, article?: IArticle}) => {
      if (res.status === "OK") {
        this.load();
      } else {
        alert(res.error);
      }
    });
  }
  
  /**
   * Load all articles
   */
  load() {
    this.apiService.get(this.defaultURI+"/articles")
    .then((articles: IArticle[]) => {
      const list = this.orderService.sortBy(articles, "asc", "name");
      this.articles = [...list];
      this._shadowed = [...list];

      this.favouriteArticles = this.articles.filter((item: IArticle) => {
        return item.is_favourite_count > 0;
      });
    });
  }

  /**
   * Search for text in title, link and tags
   */
  search() {
    console.log(this.searchText);
    if (this.searchText === "") {
      this.articles = [...this._shadowed];
    } else {
      const founded = this._shadowed.filter((item: IArticle) => {
        return item.tags.indexOf(this.searchText) !== -1 || item.title.indexOf(this.searchText) !== -1 || item.link.indexOf(this.searchText) !== -1;
      });

      this.articles = [...founded];
    }
  }

  /**
   * Sort articles via custom column, default name
   * @param column column to sorted by
   */
  nextSort(column: string = "name") {
    this.sorting = this.sorting==="asc" ? "desc" : "asc";
    const sorted = this.orderService.sortBy(this.articles, this.sorting, "name");
    this.articles = [...sorted];
  }

  /**
   * Add or remove article from favourites
   * @param article
   */
  selected(article: IArticle) {

    const exists = this.favouriteArticles.findIndex((item: IArticle) => {
      return item.id === article.id;
    });

    /** Check if article is favourite, if not add it to backed and update favouriteArticles
     * otherwise, remove from backend and from selecedarticles
    */
    if (exists > -1) {
      this.apiService.delete(this.defaultURI+"/favourite/1/"+article.id)
      .then((res: {status: string, error?: string}) => {
        if (res.status === "OK") {
          this.favouriteArticles.splice(exists, 1);
        } else {
          alert(res.error);
        }
      });
    } else { 
      this.apiService.put(this.defaultURI+"/favourite", {user_id: 1, article_id: article.id})
      .then((res: {status: string, error?: string}) => {
        if (res.status === "OK") {
          this.favouriteArticles.push(article);
        } else {
          alert(res.error);
        }
      });
    }
  }

  /**
   * Check if article is favourite
   * @param article 
   */
  isFavourite(article: IArticle) {
    const favourite = this.favouriteArticles.findIndex((item: IArticle) => {
      return item.id === article.id;
    });

    return favourite != -1 ? "checked" : "";
  }
}
