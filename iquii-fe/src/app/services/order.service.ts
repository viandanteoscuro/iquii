import { Injectable } from '@angular/core';
import { IArticle } from '../modules/article-list/types';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor() { }

  /** Sort function, if no column return full list */
  sortBy(list: IArticle[], dir: string = "asc", column:string = "name"): IArticle[] {
    if (column === '') return list; // no order
    
    const sorted = list
        .sort((a: IArticle, b: IArticle) => {
          if (this.hasKey(a, column) && this.hasKey(b, column)) {
            if (a[column] < b[column]) return -1;
            else if (a[column] > b[column]) return 1;
            else return 0;
          } else return 0;
        });

    return dir === 'asc' ? sorted : sorted.reverse();
  }

  /** handle interface index in strict mode */
  hasKey<IArticle>(obj: IArticle, key: keyof any): key is keyof IArticle {
    return key in obj
  }
}
