import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  public get(endpoint: string): Promise<any> {
    return this.http.get(endpoint).toPromise();
  }

  public post(endpoint: string, data: any): Promise<any> {
    return this.http.post(endpoint, data).toPromise();
  }

  public put(endpoint: string, data: any): Promise<any> {
    return this.http.put(endpoint, data).toPromise();
  }

  public delete(endpoint: string): Promise<any> {
    return this.http.delete(endpoint).toPromise();
  }
}
