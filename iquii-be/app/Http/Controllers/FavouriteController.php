<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Favourite;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class FavouriteController extends Controller
{
    public function add(Request $request)
    {

        $rules = [
            "user_id" => "required|integer",
            "article_id" => "required|integer"
        ];

        $valid = Validator::make($request->all(), $rules);

        /** Validating input */
        if ($valid->failed()) {
            Log::error(__CLASS__ . " - " . __METHOD__ . " Missing or wrong parameters");
            return response()->json(["status" => "KO", "error" => "Missing or wrong params"], Response::HTTP_OK);
        }
        Log::info(__CLASS__ . " - " . __METHOD__ . " Validation ok");

        $params = $request->only("user_id", "article_id");

        try {
            $fav = new Favourite();

            $fav->user_id = $params["user_id"];
            $fav->article_id = $params["article_id"];
            $fav->save();

            return response()->json(["status" => "OK"], Response::HTTP_OK);
        } catch (Exception $e) {
            Log::error(__CLASS__ . " - " . __METHOD__ . " - " . $e->getMessage());
            return response()->json(["status" => "KO", "error" => "There was en error adding favourite"], Response::HTTP_OK);
        }
    }

    public function remove(int $user_id, int $article_id)
    {
        try {
            $fav = 'App\Models\Favourite'::where("user_id", $user_id)->where("article_id", $article_id)->first();

            if ($fav) {
                $fav->delete();
            } else {
                return response()->json(["status" => "KO", "error" => "No favourite found"], Response::HTTP_OK);
            }

            return response()->json(["status" => "OK"], Response::HTTP_OK);
        } catch (Exception $e) {
            Log::error(__CLASS__ . " - " . __METHOD__ . " - " . $e->getMessage());
            return response()->json(["status" => "KO", "error" => "There was en error deleting favourite"], Response::HTTP_OK);
        }
    }
}
