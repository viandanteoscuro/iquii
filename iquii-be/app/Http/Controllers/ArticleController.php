<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{

    /**
     * Create an Article Controller
     * 
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get all articles with favourites
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        Log::info("Get all Articles");
        return response()->json('App\Models\Article'::withCount("isFavourite")->get(), Response::HTTP_OK);
    }

    /**
     * Add an article
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        Log::info("Add new Article");

        /**Setting validation rules */
        $rules = [
            "name" => "required|string",
            "title" => "required|string",
            "link" => "required|string"
        ];

        $valid = Validator::make($request->all(), $rules);

        /** Validating input */
        if ($valid->failed()) {
            Log::error(__CLASS__ . " - " . __METHOD__ . " Missing or wrong parameters");

            return response()->json(["status" => "KO", "error" => "Missing or wrong parameters"], Response::HTTP_OK);
        }
        Log::info(__CLASS__ . " - " . __METHOD__ . " Validation ok");

        try {
            $data = $request->all();

            /** Add new article */
            $article = new Article();
            $article->name = $data["name"];
            $article->link = $data["link"];
            $article->tags = $data["tags"];
            $article->title = $data["title"];
            $article->save();

            return response()->json(["status" => "OK", "article" => $article->toArray()], Response::HTTP_OK);
        } catch (Exception $e) {
            Log::error(__CLASS__ . " - " . __METHOD__ . " - " . $e->getMessage());
            return response()->json(["status" => "KO", "error" => "There was en error adding article"], Response::HTTP_OK);
        }
    }
}
