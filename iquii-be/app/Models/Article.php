<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * Relation with Favourites
     */
    public function isFavourite()
    {
        return $this->hasMany('App\Models\Favourite', 'article_id', 'id');
    }
}
