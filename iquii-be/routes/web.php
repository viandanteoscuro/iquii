<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// GET routes
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get("/articles", "ArticleController@all");

// PUT routes
$router->put("/articles", "ArticleController@add");
$router->put("/favourite", "FavouriteController@add");

// DELETE routes
$router->delete("/favourite/{user_id}/{article_id}", "FavouriteController@remove");
